import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class UserFrom extends StatefulWidget {
  String userId;
  UserForm({Key? key, required this.userId}) : super(key: key);

  @override
  _UserFormState createState() => _UserFormState(userId);
}

class _UserFromState extends State<UserFrom> {
  String userId = '';
  String fullName = '';
  String company = '';
  String age = '';

  CollectionReference users = FirebaseFirestore.instance.collection('users');
  _UserFormState(this.userId);
  TextEditingController _fulNameController = new TextEditingController();
  TextEditingController _companyController = new TextEditingController();
  TextEditingController _ageController = new TextEditingController();

  void initState() {
    super.initState();
    if (this.userId.isNotEmpty) {
      users.doc(this.userId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data()! as Map<String, dynamic>;
          fullName = data['full_name'];
          company = data['company'];
          age = data['age'].toString();
          _fulNameController.text = fullName;
          _companyController.text = company;
          _ageController.text = age;
        }
      });
    }
  }

  final _forKey = GlobalKey<FormState>();

  Future<void> addUser() {
    return users
        .add({
          'full_name': this.fullName,
          'company': this.company,
          'age': int.parse(age)
        })
        .then((value) => print("User Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> updateUser() {
    return users
        .doc(this.userId)
        .update({
          'company': company,
          'full_name': this.fullName,
          'age': int.parse(age)
        })
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        appBar: AppBar(title: Text('User')),
        body: From(
            Key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  initialValue: fullName,
                  decoration: InputDecoration(labelText: 'Full Name'),
                  onChanged: (value) {
                  setState(() {
                  fullName = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please input Name';
                    }
                    return null;
                  }
                ),
                TextFormField(
                  initialValue: company,
                  decoration: InputDecoration(labelText: 'Company'),
                  onChanged: (value) {
                  setState(() {
                  company = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please input Company';
                    }
                    return null;
                  }
                ),
                TextFormField(
                  initialValue: age,
                  decoration: InputDecoration(labelText: 'Age'),
                  onChanged: (value) {
                  setState(() {
                  age = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please input Age';
                    }
                    return null;
                  }
                ),
                ElevatedButton(onPressed: () {
                  onPressed: () async {
                  if (_forKey.currentState!.validate()) {
                    if (userId.isEmpty) {
                      await addUser();
                    } else {
                      await updateUser();
                    }
                    Navigator.pop(context);
                  }
                },
                child: Text('Save'))
          ],
        ),
      ),
    );
  }
}

